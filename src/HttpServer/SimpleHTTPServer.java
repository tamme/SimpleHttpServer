package HttpServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.charset.StandardCharsets.UTF_8;

public class SimpleHTTPServer {

    public static void main(String args[] ) throws Exception {
        ServerSocket server = new ServerSocket(8080);
        while (true) {
            clientConnection(server);
        }
    }

    private static void clientConnection(ServerSocket server) {
        try (Socket clientSocket = server.accept(); BufferedReader reader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream())) ){
            String line = reader.readLine();
            String requestType;
            if ((requestType = line.split(" ")[0]) != null){
                Path path = Paths.get("src"+ line.split("/")[1]);
                if (path.toFile().exists()) {
                  byte[] myBody = Files.readAllBytes(path);
                    String respond = "HTTP/1.1 200 OK \ncontent-type: " + Files.probeContentType(path) + "\ncontent-length: "+ myBody.length + "\n\n";
                    clientSocket.getOutputStream().write(respond.getBytes(UTF_8));
                    if ("GET".equals(requestType)) clientSocket.getOutputStream().write(myBody);
                } else {
                    String respond = "HTTP/1.1 404 Not Found \n\n 404 not found";
                    clientSocket.getOutputStream().write(respond.getBytes(UTF_8));
                }
            }
        } catch(IOException e) {
            e.printStackTrace();
        }
    }


}