package com.codeborne.httpserver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;


public class ClientConnection {

    Socket socket;

    public ClientConnection(Socket socket) throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        Request request = new Request(parseHeaders(bf));
        request.setRequestBody(parseBody(bf, request.getContentLength()));
        Response response = getResponse(request);
        clientSocket(socket, response);
    }

    public void clientSocket(Socket socket, Response response) throws IOException {
        socket.getOutputStream().write(response.getHeaderAndBody());
    }


    public ArrayList<String> parseHeaders(BufferedReader bf) throws IOException {

        ArrayList<String> headers = new ArrayList<String>();
        String headerLine;
        while ((headerLine = bf.readLine()).length() > 0) {
            headers.add(headerLine);
        }
        return headers;
    }

    char[] parseBody(BufferedReader bf, int contentLength) throws IOException {
        char[] bodyContent = new char[contentLength];


        int readAll = 0;
        while (readAll < contentLength ){

            int readChunk = bf.read(bodyContent, readAll, contentLength - readAll);

            if (readChunk > -1) {
                readAll += readChunk;
            }
            else {break;}

        }
        return bodyContent;
    }

    Response getResponse(Request request) {
        Response response;
        if ("GET".equals(request.getRequestType())) {
            response = new GetResponse(request);
        } else if ("HEAD".equals(request.getRequestType())) {
            response = new HeadResponse(request);
        } else if ("POST".equals(request.getRequestType())) {
            response = new PostResponse(request);
        } else response = new OtherResponse(request);
        return response;

    }
}
