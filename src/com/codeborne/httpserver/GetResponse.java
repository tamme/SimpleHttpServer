package com.codeborne.httpserver;

import java.io.IOException;
import java.nio.file.Files;

public class GetResponse extends HeadResponse{
    Request request;
    public GetResponse(Request request) {
        super(request);
        this.request = request;
    }

    @Override
    public byte[] getBody() {
        byte[]body = new byte[0];
        if (request.getPath().toFile().isFile())
        {
            try {
                return Files.readAllBytes(request.getPath());
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("fail on");
        }
        return body;
    }

}
