package com.codeborne.httpserver;

import java.io.IOException;
import java.nio.file.Files;
import java.util.LinkedHashMap;
import java.util.Map;

public class HeadResponse extends Response {

    public HeadResponse(Request request) {
        super(request);
    }

    @Override
    public byte[] getBody() {
        byte[]body = new byte[0];
        return body;
    }



    @Override
    public Map<String, Object> getHeaders() {
        Map<String, Object> header = new LinkedHashMap<>();
        if (request.isExistsStatus()) {
            try {
                header.put("content-type", Files.probeContentType(request.getPath()));
                header.put("content-length", String.valueOf(request.getPath().toFile().length()));

            } catch (IOException e) {
                e.printStackTrace();
            }
            return header;
        }


        return header;
    }

    @Override
    public String getStatusCode() {
        if (request.isExistsStatus()) return "200 OK";
        else return "404 Not Found";
    }
}
