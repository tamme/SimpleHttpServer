package com.codeborne.httpserver;

import java.util.LinkedHashMap;
import java.util.Map;

public class OtherResponse extends Response{

    public OtherResponse(Request request) {
        super(request);
    }

    @Override
    public byte[] getBody() {
        return "Bad request 400".getBytes();
    }

    @Override
    public Map<String, Object> getHeaders() {
        return new LinkedHashMap<>();

    }

    @Override
    public String getStatusCode() {
        return "400 Bad Request";
    }
}
