package com.codeborne.httpserver;

import java.util.LinkedHashMap;
import java.util.Map;

public class PostResponse extends Response{
    public PostResponse(Request request) {
        super(request);
    }

    @Override
    public byte[] getBody() {

        return "post request".getBytes();
    }

    @Override
    public Map<String, Object> getHeaders() {
        Map<String, Object> header = new LinkedHashMap<>();
        return header;
    }

    @Override
    public String getStatusCode() {
        return "200 OK";
    }

}
