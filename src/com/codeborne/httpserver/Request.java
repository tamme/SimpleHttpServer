package com.codeborne.httpserver;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Request {

    private String requestType;
    Path path = Paths.get("src");
    private Map<String, String> requestHeaders = new LinkedHashMap<>();
    private char[] requestBody;

    public Request(List<String> requestHeadersArray) {
        String line = requestHeadersArray.get(0);
        if (requestHeadersArray.get(0).split(" ").length > 2) {
            this.requestType = line.split(" ")[0];
            this.path = Paths.get("src",
                    line.split(" ")[1]);
        }
        setRequestHeaders(requestHeadersArray);
    }

    public Request() {
    }


    public Map setRequestHeaders(List<String> requestHeadersAndBodyArray) {
        String[] pairs;

        for (int i = 1; i < requestHeadersAndBodyArray.size(); i++) {
            if ((pairs = requestHeadersAndBodyArray.get(i).split(":", 2)).length > 1) {
                requestHeaders.put(pairs[0].trim(), pairs[1].trim());
            }
        }
        return requestHeaders;
    }


    String getRequestType() {
        return requestType;
    }

    Path getPath() {
        return path;
    }

    boolean isExistsStatus() {
        return this.path.toFile().exists() && this.path.toFile().isFile();
    }

    public Map<String, String> getRequestHeaders() {
        return requestHeaders;
    }

    int getContentLength() {
        return Integer.valueOf(requestHeaders.get("Content-Length").trim());
    }

    public void setRequestBody(char[] requestBody) {
        this.requestBody = requestBody;
    }


    public char[] getRequestBody() {
        return requestBody;
    }
}
