package com.codeborne.httpserver;

import java.io.IOException;
import java.util.Map;

public abstract class Response {

    Request request;

    public Response(Request request) {
        this.request = request;
    }



    byte[] getHeaderAndBody() throws IOException {
        String headersAndInitialLine;
        headersAndInitialLine = "HTTP/1.1 "+ getStatusCode() + "\n";

        for (Map.Entry<String, Object> headerLine : getHeaders().entrySet()) {
            headersAndInitialLine = headersAndInitialLine + headerLine.getKey() + ": " + headerLine.getValue() + "\n";
        }
        headersAndInitialLine = headersAndInitialLine + "\n";
        byte[] headerArray = headersAndInitialLine.getBytes();
        byte[] headerAndBody = new byte[headerArray.length + getBody().length];
        System.arraycopy(headerArray, 0 , headerAndBody, 0, headerArray.length);
        System.arraycopy(getBody(), 0, headerAndBody, headerArray.length, getBody().length);
        return headerAndBody;
    }



    public abstract byte[] getBody() throws IOException;
    public abstract Map<String, Object> getHeaders() throws IOException;
    public abstract String getStatusCode();
}


