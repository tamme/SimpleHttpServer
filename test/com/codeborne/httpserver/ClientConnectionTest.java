package com.codeborne.httpserver;

import org.junit.Test;
import org.mockito.Answers;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertArrayEquals;
import static org.mockito.Mockito.*;

public class ClientConnectionTest {


    @Test
    public void getResponseGet() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        when(request.getRequestType()).thenReturn("GET");
        Response response = clientConnection.getResponse(request);
        assertTrue(response instanceof GetResponse);
    }

    @Test
    public void getResponseHead() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        when(request.getRequestType()).thenReturn("HEAD");
        Response response = clientConnection.getResponse(request);
        assertTrue(response instanceof HeadResponse);
    }


    @Test
    public void getResponseOther() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        when(request.getRequestType()).thenReturn("LINK");
        Response response = clientConnection.getResponse(request);
        assertTrue(response instanceof OtherResponse);
    }

    @Test
    public void getResponsePost() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        Request request = mock(Request.class);
        when(request.getRequestType()).thenReturn("POST");
        Response response = clientConnection.getResponse(request);
        assertTrue(response instanceof PostResponse);
    }

    @Test
    public void clientSocket() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        Socket socket = mock(Socket.class);
        Response response = mock(Response.class);
        ByteArrayOutputStream byteArrayOutputStream = mock(ByteArrayOutputStream.class);

        doReturn("test".getBytes()).when(response).getHeaderAndBody();
        doReturn(byteArrayOutputStream).when(socket).getOutputStream();

        clientConnection.clientSocket(socket, response);
        verify(byteArrayOutputStream).write("test".getBytes());
    }


    @Test
    public void parseHeaders() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class);
        BufferedReader preparedBr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("esimene rida\nteine rida\nkolmas rida\n\n".getBytes())));
        doCallRealMethod().when(clientConnection).parseHeaders(preparedBr);
        ArrayList<String> expected = new ArrayList<String>(Arrays.asList("esimene rida", "teine rida", "kolmas rida"));
        ArrayList<String> result = clientConnection.parseHeaders(preparedBr);
        assertEquals(expected, result);
    }

    @Test
    public void parseBody() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class);
        BufferedReader preparedBr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("abcdefghij".getBytes())));
        int preparedBodyLength = 10;
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);
        char[] expected = new char[] {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j'};
        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);
        assertArrayEquals(expected, result);
    }

    @Test
    public void parseBodyWith0Length() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class);
        BufferedReader preparedBr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("abcdefghij".getBytes())));
        int preparedBodyLength = 0;
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);

        char[] expected = new char[0];
        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);

        assertArrayEquals(expected, result);
    }

    @Test
    public void parseBodyWithSmallerLength() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class);
        BufferedReader preparedBr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("abcdefghij".getBytes())));
        int preparedBodyLength = 5;
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);

        char[] expected = new char[] {'a', 'b', 'c', 'd', 'e'};
        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);

        assertArrayEquals(expected, result);
    }

    @Test
    public void parseBodyWithLargerLength() throws IOException {
        ClientConnection clientConnection = mock(ClientConnection.class);
        BufferedReader preparedBr = new BufferedReader(new InputStreamReader(new ByteArrayInputStream("ab".getBytes())));
        int preparedBodyLength = 5;
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);
        char[] expected = new char[5];
        expected[0] = 'a';
        expected[1] = 'b';
        //expected = new char[] {'a', 'b','a', 'b','a'};
        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);
        assertEquals(expected.length, result.length);
        assertArrayEquals(expected, result);
    }


    @Test
    public void parseBodyChunk() throws IOException {
        int preparedBodyLength = 5;
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        BufferedReader preparedBr = mock(BufferedReader.class);

        doReturn(2).doReturn(2).doReturn(1).when(preparedBr).read(any(), anyInt(), anyInt());

        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);

        assertEquals(5, result.length);
        verify(preparedBr).read(result, 0, 5);
        verify(preparedBr).read(result, 2, 3);
        verify(preparedBr).read(result, 4, 1);
        verifyNoMoreInteractions(preparedBr);
        assertEquals(5, result.length);
        inOrder((preparedBr).read(result, 0, 5), (preparedBr).read(result, 2, 3), (preparedBr).read(result, 4, 1));
    }

    @Test
    public void parseBodyChunkBodyLenghtMoreThanActual() throws IOException {
        int preparedBodyLength = 5;
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        BufferedReader preparedBr = mock(BufferedReader.class);
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);
        doReturn(2).doReturn(2).doReturn(2).doReturn(2).when(preparedBr).read(any(), anyInt(), anyInt());

        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);

        assertEquals(5, result.length);
        verify(preparedBr).read(result, 0, 5);
        verify(preparedBr).read(result, 2, 3);
        verify(preparedBr).read(result, 4, 1);
        assertEquals(5, result.length);
        verifyNoMoreInteractions(preparedBr);
    }

    @Test
    public void parseBodyChunkBodyLenghtLessThanActual() throws IOException {
        int preparedBodyLength = 5;
        ClientConnection clientConnection = mock(ClientConnection.class, Answers.CALLS_REAL_METHODS);
        BufferedReader preparedBr = mock(BufferedReader.class);
        doCallRealMethod().when(clientConnection).parseBody(preparedBr, preparedBodyLength);
        doReturn(2).doReturn(2).doReturn(-1).when(preparedBr).read(any(), anyInt(), anyInt());

        char[] result = clientConnection.parseBody(preparedBr, preparedBodyLength);

        assertEquals(5, result.length);
        verify(preparedBr).read(result, 0, 5);
        verify(preparedBr).read(result, 2, 3);
        verify(preparedBr).read(result, 4, 1);
        assertEquals(5, result.length);
        verifyNoMoreInteractions(preparedBr);
    }


}