package com.codeborne.httpserver;


import org.junit.Test;

import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class GetResponseTest {

    @Test
    public void getBody() throws Exception {
        Request request = mock(Request.class);
        when(request.getPath()).thenReturn(Paths.get("test", "test.txt"));
        request.path = Paths.get("test", "test.txt");
        GetResponse response = new GetResponse(request);
        assertEquals("Esimene rida", new String(response.getBody()));
        }


}