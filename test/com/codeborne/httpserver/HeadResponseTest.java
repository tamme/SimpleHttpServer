package com.codeborne.httpserver;

import org.junit.Test;

import java.nio.file.Paths;
import java.util.Map;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class HeadResponseTest {


    @Test
    public void getBody() {
        HeadResponse headResponse = new HeadResponse(mock(Request.class));
        assertArrayEquals(new byte[0], headResponse.getBody());
    }



    @Test
    public void getHeaders() {
        Request request = mock(Request.class);
        when(request.isExistsStatus()).thenReturn(true);
        when(request.getPath()).thenReturn(Paths.get("test", "test.txt"));
        HeadResponse headResponse = new HeadResponse(request);
        String headersString = "";
        for (Map.Entry<String, Object> headerLine : headResponse.getHeaders().entrySet()) {
            headersString = headersString + headerLine.getKey() + ": " + headerLine.getValue() + "\n";
        }
        assertEquals("content-type: text/plain\ncontent-length: 12\n", headersString);
    }


    @Test
    public void getStatusCodeFileFound() {
        Request request = mock(Request.class);
        when(request.isExistsStatus()).thenReturn(true);
        HeadResponse headResponse = new HeadResponse(request);
        assertEquals("200 OK", headResponse.getStatusCode());
    }

    @Test
    public void getStatusCodeFileNotFound() {
        Request request = mock(Request.class);
        when(request.isExistsStatus()).thenReturn(false);
        HeadResponse headResponse = new HeadResponse(request);
        assertEquals("404 Not Found", headResponse.getStatusCode());
    }

}