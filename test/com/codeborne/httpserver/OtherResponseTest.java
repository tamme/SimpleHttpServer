package com.codeborne.httpserver;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class OtherResponseTest {
    Request request = mock(Request.class);
    OtherResponse otherResponse = new OtherResponse(request);


    @Test
    public void getStatusCode() throws IOException {
        assertEquals("400 Bad Request", otherResponse.getStatusCode());
    }

    @Test
    public void getHeaders() throws IOException {
        assertEquals(0, otherResponse.getHeaders().size());
    }

    @Test
    public void getBody() throws IOException {
        assertEquals("Bad request 400", new String(otherResponse.getBody()));
    }



}