package com.codeborne.httpserver;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class PostResponseTest {
    Request request = mock(Request.class);
    PostResponse postResponse = new PostResponse(request);
    @Test

    public void getBody() throws IOException {
        assertEquals("post request", new String(postResponse.getBody()));
    }

    @Test
    public void getHeaders() throws IOException {
        assertEquals(0, postResponse.getHeaders().size());
    }

    @Test
    public void getStatusCode() throws IOException {
        assertEquals("200 OK", postResponse.getStatusCode());
    }

}