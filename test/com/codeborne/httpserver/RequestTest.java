package com.codeborne.httpserver;

import org.junit.Test;

import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;
import static org.mockito.Mockito.*;

public class RequestTest {

    @Test
    public void getRequestTypeWithGET() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("GET /pilt.jpg HTTP.."));
        Request request = new Request(requestHeaders);
        assertEquals("GET", request.getRequestType());
    }

    @Test
    public void getRequestTypeWithHEAD() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("HEAD /pilt.jpg HTTP.."));
        Request request = new Request(requestHeaders);
        assertEquals("HEAD", request.getRequestType());
    }

    @Test
    public void getRequestTypeWithEmptyLine() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList(""));
        Request request = new Request(requestHeaders);
        assertEquals(null, request.getRequestType());
    }
    @Test
    public void getRequestTypeWithLineWithoutSpaces() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("asdadasdsadsada"));
        Request request = new Request(requestHeaders);
        assertEquals(null, request.getRequestType());
    }

    @Test
    public void getRequestTypeWithPost() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("POST /text.txt"));
        Request request = new Request(requestHeaders);
        assertEquals(null, request.getRequestType());
    }


    @Test
    public void isExistsStatusWithLineWithWrongFileName() {
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("POST /pic.jpg HTTP.."));
        Request request = new Request(requestHeaders);
        assertEquals(false, request.isExistsStatus());
    }

    @Test
    public void isExistsStatusWithLineWithRightFileName() {
        Request request = mock(Request.class);
        request.path = Paths.get("test/test.txt");
        doCallRealMethod().when(request).isExistsStatus();
        assertEquals(true, request.isExistsStatus());
    }

    @Test
    public void getPath() {
        String firstline = "GET /pilt.jpg HTTP..";
        ArrayList<String> requestHeaders = new ArrayList<>(Arrays.asList("POST /pilt.jpg HTTP.."));
        Request request = new Request(requestHeaders);

        assertEquals(Paths.get("src/pilt.jpg"), request.getPath());
    }

    @Test
    public void setRequestHeaders() {
        Map<String,String> expectedMap = new HashMap<>();
        expectedMap.put("Header1", "emailto:xxx@gmail.com");
        expectedMap.put("Header2", "Value2");
        ArrayList<String> requestHeaders = new ArrayList<String>(Arrays.asList("POST /pic.jpg HTTP..", "Header1:  emailto:xxx@gmail.com", "Header2: Value2"));
        Request request = new Request();
        assertEquals(2, request.setRequestHeaders(requestHeaders).size());
        assertEquals(expectedMap.get("Header1"), request.setRequestHeaders(requestHeaders).get("Header1"));
        assertEquals(expectedMap.get("Header2"), request.setRequestHeaders(requestHeaders).get("Header2"));
        assertTrue(expectedMap.equals(request.setRequestHeaders(requestHeaders)));
    }

/*
    @Test
    public void setRequestBody() {
       List<String> expected = new ArrayList<>(Arrays.asList("body1","body2", "", "body3"));
       Request request = new Request();
       List<String> requestHeadersAndBody = new ArrayList<>(Arrays.asList("POST /pic.jpg HTTP..", "Header1:  emailto:xxx@gmail.com", "\n\n", "body1", "body2", "", "body3"));
       assertEquals(expected, request.setRequestBody(requestHeadersAndBody));
    }
    */
}