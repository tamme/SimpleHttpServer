package com.codeborne.httpserver;

import org.junit.Test;

import java.io.IOException;
import java.util.Collections;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class ResponseTest {

    @Test
    public void getHeaderAndBody2() throws IOException {
        Map header = Collections.singletonMap("key", "value");
        byte[] body = "body".getBytes();

        Response response = mock(Response.class, CALLS_REAL_METHODS);
        when(response.getHeaders()).thenReturn(header);
        when(response.getBody()).thenReturn(body);
        when(response.getStatusCode()).thenReturn("200 OK");
        assertEquals("HTTP/1.1 200 OK\nkey: value\n\nbody", new String(response.getHeaderAndBody()));
    }
}

